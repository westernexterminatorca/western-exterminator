Whether your pest control issue is ants, rodents, bed bugs, termites, cockroaches, or other pest, Western Exterminator has a highly trained, licensed and state-certified pest control specialist who can help both home and businesses owners.

Address: 1160 Sandhill Ave, Carson, CA 90746

Phone: 310-645-1116

Website: https://www.westernexterminator.com/carson/
